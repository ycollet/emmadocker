Install docker (from https://docs.docker.com/engine/install/fedora/):
```
$ dnf -y install dnf-plugins-core
$ dnf config-manager --add-repo https://download.docker.com/linux/fedora/docker-ce.repo
$ dnf install docker-ce docker-ce-cli containerd.io
```

On Fedora (probably on other systems too), start the docker service:
```
$ sudo systemctl start docker
$ usermod -a -G docker <MY USER ID>
```

You will need to logout / login after adding your username to the docker group.

To clean up:
```
$ docker ps -a
$ docker rm <CONTAINER ID 1> <CONTAINER ID 2> ...
$ docker images
$ docker rmi <IMAGE ID 1> <IMAGE ID 2> ...
```

To remove all images:
```
$ docker image prune -a
```

You can customize the docker base image using one on Docker hub:
https://hub.docker.com/_/debian

The docker base image is currently set to:
```
FROM debian:bullseye-slim
```

To build the container:
```
$ docker build -t emma .
```

To run the container:
```
$ docker run -t -i --cap-add SYS_ADMIN -v <PATH TO EMMABUNTUS GIT REPO>:/builddir emma
```

Inside the container:
```
$ cd /builddir
$ make 64b
$ make clean64b
$ make cleanfull
$ git clean -xdn
$ git clean -xdf
```

```
$ make 32b
$ make clean32b
$ make cleanfull
$ git clean -xdn
$ git clean -xdf
```

To test the ISO file:

Install QEmu-KVM and the SDL interface.

```
$ dnf install qemu-system-x86-core qemu-kvm
$ dnf install qemu-ui-sdl qemu-audio-sdl
```

In the emmabuntus git repository (the iso files are written here):

Without audio:
```
$ qemu-kvm -m 2048 -vga qxl -sdl -cdrom emmabuntus-de4-amd64-11.0-1.00.iso
```
With audio and usb:
```
$ qemu-kvm -m 2048 -vga qxl -usb -soundhw hda -sdl -cdrom emmabuntus-de4-amd64-11.0-1.00.iso
```
With audio, usb and with 2 cpus:
```
$ qemu-kvm -m 2048 -vga qxl -usb -soundhw hda -smp cpus=2 -sdl -cdrom emmabuntus-de4-amd64-11.0-1.00.iso
```
