FROM debian:bullseye-slim
LABEL maintainer="ycollette.nospam@free.fr"
ENV DEBIAN_FRONTEND noninteractive
VOLUME /builddir
RUN apt-get update && apt-get install -y apt-cacher-ng make live-build live-manual live-tools git dpkg-dev xorriso isolinux
WORKDIR /builddir
ENTRYPOINT [ "bash" ]
